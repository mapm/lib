//! Source for the LaTeX package that allows mapm to work under the hood

pub const MAPM_STY: &str = r#"\ProvidesPackage{mapm}

\input{mapm-headers.tex}

\newcommand\mapmvar[1]{\csname mapm@var@#1\endcsname}
\newcommand\probcount{\csname mapm@probcount\endcsname}
\newcommand\probname[1]{\csname mapm@probname@#1\endcsname}
\newcommand\solcount[1]{\csname mapm@solcount@#1\endcsname}
\newcommand\probvar[2]{\input{mapm-prob-#1-#2.tex}\unskip}
\newcommand\solvar[3]{\input{mapm-sol-#1-#2-#3.tex}\unskip}
"#;
