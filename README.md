# The mapm project has been moved to [sr.ht](https://sr.ht/~dennisc/mapm/).

# mapm

This is the library that the mapm binaries use for their functionality.

mapm is a problem-management software that allows you to quickly iterate through drafts of exams. By modularizing your problems into their own files, you can easily mix and match problems and compile exams and solution manuals.

By using built-in TeX macros and a sane implementation of the incredibly human-readable YAML configuration language, you can spend less time learning how to organize your exam creation process and more time on the exam itself.

Read the library documentation for mapm at [docs.rs/mapm](https://docs.rs/mapm).

## Is this for me?

mapm is meant for you if you are an exam-creator or contest writer whose template has a **fixed** number of problems. Thus, contests like the AMCs can easily be typeset with mapm, while exams like the SAT or school tests with a variable number of questions do not have the same level of support.
