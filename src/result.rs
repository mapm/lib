//! Contains the custom mapm error type

pub type MapmResult<T> = Result<T, MapmErr>;

#[derive(Debug)]
pub enum MapmErr {
    ProblemErr(String),
    SolutionErr(String),
    ContestErr(String),
    TemplateErr(String),
}
