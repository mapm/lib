use std::fs;
use std::path::Path;

/// Copies a directory into a destination directory,
/// ignoring any "dotfiles" which begin with a `.`
///
/// If the `to` directory does not exist, it will be created.
/// If it does exist, files inside will be overwritten,
/// but any remaining residual files will not be deleted.
///
/// If `to` exists but is not a directory (for instance, if it is a file),
/// this function will return an error.
///
/// You should always have `originator` be `true` when calling the function yourself,
/// `false` is for recursive calls.

pub(crate) fn copy_dir_ignore_dots<P: AsRef<Path>, Q: AsRef<Path>>(from: P, to: Q) -> std::io::Result<()>
{
    let from = from.as_ref();
    let to = to.as_ref();
    if !to.is_dir() {
        if to.exists() {
            let err = &format!(
                "Destination path {:?} exists but is a directory; please move it to fix this issue.",
                to,
            );
            return Err(std::io::Error::new(std::io::ErrorKind::Other, err.as_str()));
        }
        if let Err(e) = std::fs::create_dir(&to) {
            return Err(e);
        };
    }
    for entry in fs::read_dir(from)? {
        let entry = entry?;
        let path = &entry.path();
        let file_name = path.file_name().unwrap().to_str().unwrap();
        if &file_name[..1] != "." {
            let destination = &to.join(file_name);
            if path.is_file() || path.is_symlink() {
                fs::copy(path, destination)
                    .unwrap_or_else(|_| panic!("Failed to copy {:?} to {:?}", path, destination));
            } else {
                if !destination.is_dir() {
                    if destination.exists() {
                        let err = &format!(
                        "Destination path {:?} exists but is a directory; please move it to fix this issue.",
                        destination,
                    );
                        return Err(std::io::Error::new(std::io::ErrorKind::Other, err.as_str()));
                    }
                    if let Err(e) = std::fs::create_dir(destination) {
                        return Err(e);
                    };
                }
                copy_dir_ignore_dots(path, destination)?
            }
        }
    }
    Ok(())
}
