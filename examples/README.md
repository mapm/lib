# mapm examples

In this directory are the most barebone examples of mapm possible.

For some real-world examples, check out the [Math Advance contest releases](https://gitlab.com/mathadvance/contest-releases). One of the contests in the mapm docs is based on MAT 2021, which can be found inside the releases.
